from datetime import datetime
from typing import Dict

from extensions import db
from model.user import UserModel


class TodoModel(db.Model):
    __tablename__ = 'todo'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=True)
    description = db.Column(db.String, nullable=True)
    status = db.Column(db.SMALLINT)
    user = db.Column(db.Integer, db.ForeignKey(UserModel.id))
    due_date = db.Column(db.DateTime, nullable=True)

    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True)


    def to_dict(self) -> Dict:
        result = {}
        result.update(
            {
                "id": self.id,
                "title": self.title,
                "status": self.status,
                "description": self.description,
                "due_date": str(self.due_date.strftime('%d-%m-%Y'))
            }
        )
        return result

    @classmethod
    def get_by_id(cls, _id):
        return cls.query.filter(cls.id == _id)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()



