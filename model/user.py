from datetime import datetime
from typing import Dict
from enums.user import UserStatusEnum
from extensions import db


class UserModel(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    password = db.Column(db.String(240), nullable=False)
    username = db.Column(db.String(100), nullable=False, unique=True)
    status = db.Column(db.Enum(UserStatusEnum))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True)


    def to_dict(self) -> Dict:
        result = {}
        result.update(
            {
                "id": self.id,
                "name": self.name,
                "status": self.status.name,
            }
        )
        return result
