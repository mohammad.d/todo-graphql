import graphene
from flask_graphql_auth import get_jwt_identity, query_jwt_required
from graphene_sqlalchemy import SQLAlchemyConnectionField
from sqlalchemy import cast, Date

from enums.todo import TodoEnum
from graphqls.v1.todo_mutation import ProtectedTodo
from model.todo import TodoModel
from objects_graphql import TodoObject, UserObject


class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()
    all_users = SQLAlchemyConnectionField(UserObject)
    # all_todos = SQLAlchemyConnectionField(TodoObject)

    get_todo = graphene.Field(type=ProtectedTodo, token=graphene.String(), id=graphene.Int())
    todos_list = graphene.List(of_type=ProtectedTodo, token=graphene.String())

    get_todos_by_status = graphene.List(of_type=ProtectedTodo, token=graphene.String(), status=TodoEnum())
    get_todos_by_due_date = graphene.List(of_type=ProtectedTodo, token=graphene.String(), due_date=graphene.Date())

    @query_jwt_required
    def resolve_get_todo(self, info, id):
        user_id = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        todo = todo_qry.filter(TodoModel.id == id, TodoModel.user == user_id).first()
        return todo

    @query_jwt_required
    def resolve_todos_list(self, info):
        user_id = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        todos = todo_qry.filter(TodoModel.user == user_id).all()
        return todos

    @query_jwt_required
    def resolve_get_todos_by_status(self, info, status):
        user_id = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        return todo_qry.filter(TodoModel.user == user_id, TodoModel.status == status).all()

    @query_jwt_required
    def resolve_get_todos_by_due_date(self, info, due_date):
        user_id = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        return todo_qry.filter(TodoModel.user == user_id, cast(TodoModel.due_date, Date) == due_date).all()
