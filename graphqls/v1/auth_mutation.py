import graphene
from flask_graphql_auth import create_access_token, create_refresh_token

from model.user import UserModel


class AuthMutation(graphene.Mutation):
    access_token = graphene.String()
    refresh_token = graphene.String()

    class Arguments:
        username = graphene.String()
        password = graphene.String()

    def mutate(self, info, username, password):
        user = UserModel.query.filter_by(username=username, password=password).first()
        if not user:
            raise Exception('Authenication Failure : User is not registered')
        return AuthMutation(
            access_token=create_access_token(user.id),
            refresh_token=create_refresh_token(user.id)
        )
