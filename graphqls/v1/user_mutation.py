import graphene

from model.user import UserModel
from extensions import db
from objects_graphql import UserObject


class CreateUser(graphene.Mutation):
    user = graphene.Field(UserObject)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    def mutate(self, info, username, password):
        user = UserModel.query.filter_by(username=username).first()
        if user:
            return CreateUser(user=user)
        user = UserModel(username=username, password=password)
        if user:
            db.session.add(user)
            db.session.commit()
        return CreateUser(user=user)

