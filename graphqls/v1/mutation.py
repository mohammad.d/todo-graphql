import graphene

from graphqls.v1.auth_mutation import AuthMutation
from graphqls.v1.todo_mutation import CreateTodo, UpdateTodoStatus, UpdateTodoDueDate, DeleteTodo
from graphqls.v1.user_mutation import CreateUser


class Mutation(graphene.ObjectType):
    auth = AuthMutation.Field()
    create_user = CreateUser.Field()
    protected_create_todo = CreateTodo.Field()
    update_todo_status = UpdateTodoStatus.Field()
    update_todo_due_date = UpdateTodoDueDate.Field()
    delete_todo = DeleteTodo.Field()