import graphene
from flask_graphql_auth import mutation_jwt_required, get_jwt_identity, AuthInfoField

from enums.todo import TodoEnum
from extensions import db
from model.todo import TodoModel
from objects_graphql import TodoObject


class ProtectedTodo(graphene.Union):
    class Meta:
        types = (TodoObject, AuthInfoField)


class CreateTodo(graphene.Mutation):
    todo = graphene.Field(ProtectedTodo)

    class Arguments:
        title = graphene.String(required=True)
        description = graphene.String(required=False)
        status = graphene.Argument(TodoEnum)
        token = graphene.String()
        due_date = graphene.DateTime()

    @mutation_jwt_required
    def mutate(self, info, title, description, status, due_date):
        user = get_jwt_identity()
        todo = TodoModel(title=title, user=user, description=description, status=status, due_date=due_date)
        if todo:
            todo.save_to_db()
        return CreateTodo(todo=todo)


class UpdateTodoStatus(graphene.Mutation):
    todo = graphene.Field(ProtectedTodo)

    class Arguments:
        id = graphene.ID()
        token = graphene.String()
        status = graphene.Argument(TodoEnum)

    @mutation_jwt_required
    def mutate(self, info, id, status):
        user = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        todo = todo_qry.filter(TodoModel.id == id, TodoModel.user == user).first()
        todo.status = status
        db.session.commit()
        return CreateTodo(todo=todo)


class UpdateTodoDueDate(graphene.Mutation):
    todo = graphene.Field(ProtectedTodo)

    class Arguments:
        id = graphene.ID()
        token = graphene.String()
        due_date = graphene.DateTime()

    @mutation_jwt_required
    def mutate(self, info, id, due_date):
        user = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        todo = todo_qry.filter(TodoModel.id == id, TodoModel.user == user).first()
        todo.due_date = due_date
        db.session.commit()
        return CreateTodo(todo=todo)


class DeleteTodo(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments:
        id = graphene.ID()
        token = graphene.String()

    @mutation_jwt_required
    def mutate(self, info, id):
        user = get_jwt_identity()
        todo_qry = TodoObject.get_query(info)
        todo_qry.filter(TodoModel.id == id).delete()
        db.session.commit()
        return DeleteTodo(ok=True)



