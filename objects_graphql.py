import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType

from model.todo import TodoModel
from model.user import UserModel


class UserObject(SQLAlchemyObjectType):
    class Meta:
        model = UserModel
        interfaces = (graphene.relay.Node,)


class TodoObject(SQLAlchemyObjectType):
    class Meta:
        model = TodoModel
        interfaces = (graphene.relay.Node,)
