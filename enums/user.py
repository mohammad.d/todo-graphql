from enum import Enum


class UserStatusEnum(Enum):
    Deactivated = 0
    Activated = 1

