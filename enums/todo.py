from graphene import Enum

class TodoEnum(Enum):
    created = 0
    doing = 1
    done = 3
    deleted = 4