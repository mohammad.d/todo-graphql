import os

from flask import \
    Flask
from flask.cli import load_dotenv
from flask_graphql import GraphQLView
from flask_graphql_auth import (
    GraphQLAuth,
    graphene
)

from extensions import db
from graphqls.v1.query import Query
from graphqls.v1.mutation import Mutation

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)


def create_app():
    postgres = {
        'user': os.getenv("DB_USER"),
        'pw': os.getenv("DB_PASSWORD"),
        'db': os.getenv("DB_NAME"),
        'host': os.getenv("DB_HOST"),
        'port': os.getenv("DB_PORT"),
    }

    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % postgres

    app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")
    app.config["JWT_SECRET_KEY"] = os.getenv("JWT_SECRET_KEY") # Change this!

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    return app


app = create_app()
auth = GraphQLAuth(app)
db.init_app(app)


@app.before_first_request
def create_tables():
    db.create_all()


@app.route('/')
def hello():
    return "hello"

schema = graphene.Schema(query=Query, mutation=Mutation)

app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True
    )
)
